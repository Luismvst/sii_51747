// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#include <iostream>
#include <pthread.h>

void* hilo_comandos(void* d)
{
	CMundo *p=(CMundo*)d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	while(1)
	{
		usleep(10);
		char cadena[100];
		read (tubTeclas, cadena, sizeof(cadena));
		unsigned char key;
		sscanf(cadena, "%c", &key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
	}
}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//char *proyection;
//En el servidor ya no hay comunicacion con el mapa de memoria

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	
	//Cerramos la tubería
	char cerrar[200];
	sprintf(cerrar, "__Cerrando Tenis...__");
	write (tuberia, cerrar, strlen(cerrar)+1);
	close(tuberia);
	
	//No nos hace falta en el servidor.
	//Cerramos de la proyeccion de la memoria.
	//munmap(proyection, sizeof(Swap));
	

	//Tuberia Cliente-Servidor
	close(tubCS);

	//Tuberia teclas Cliente-Servidor
	close(tubTeclas);
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	//gluPerspective( 40.0, 1270.0f/720.0f, 0.1, 150);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	extern int aux1;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		//Mensaje de la tubería
		char frase1[100];
		sprintf(frase1, "Jugador 2 marca 1 punto, lleva %d", puntos2);
		write(tuberia,frase1, strlen(frase1)+1);

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//Mensaje de la tubería
		char frase2[100];
		sprintf(frase2, "Jugador 1 marca 1 punto, lleva %d", puntos1);
		write(tuberia,frase2, strlen(frase2)+1);
	}
	
	if (jugador1.Rebota(esfera))
	{
		jugador1.y1-=0.1;
		jugador1.y2+=0.1;
		//jugador2.y1+=0.2;
	}
	if (jugador2.Rebota(esfera))
	{
		jugador2.y1-=0.1;
		jugador2.y2+=0.1;
		//jugador1.y1+=0.2;
	}
	
	//Es servidor y no se comunica con bot

	/*
	//Tambien debemos actualizar los valores reales en nuestra proyección del fichero
	pSwap->esfera=esfera;
	pSwap->raqueta1=jugador1;
	pSwap->raqueta2=jugador2;
	*/

	//Implementación sobre el bot	
	/*
	//Recibimos con un switch case el valor de la acción que se proyecta desde el bot en el neuvo fichero proyectado.
	switch(pSwap->accion)
	{
		case 1: jugador2.velocidad.y=4;	break;
		case 0: break;
		case -1: jugador2.velocidad.y=-4; break;
	}*/
	/*
	if(aux1<=0)
	{
		switch(pSwap->accion1)
		{
			case 1: jugador1.velocidad.y=4;	break;
			case 0: break;
			case -1: jugador1.velocidad.y=-4; break;
		}
	}*/

	//Finalizamos juego por limite de puntos
	char cadena[200];
	int flag1=0, flag2=0;	//Maquina Estados
	if (puntos1==3){
		flag1=1;
		if(flag1!=flag2)
		{
			sprintf(cadena, "**Jugador 1 ha marcado 3 puntos y gana la partida**");
			write (tuberia, cadena, strlen (cadena)+1);
		}
		flag2=flag1;
		exit(0);

	}
	if (puntos2==3){
		flag1=1;	
		if(flag1!=flag2)
		{
			sprintf(cadena, "**Jugador 2 ha marcado 3 puntos y gana la partida**");
			write (tuberia, cadena, strlen (cadena)+1);
		}
		flag2=flag1;
		exit(0);
	}

	//Comunicacion Cliente-Servidor

	char cadCS[200];
	sprintf(cadCS, "%f %f %f %f %f %f %f %f %f %f %d %d", 
		esfera.centro.x,esfera.centro.y,
		jugador1.x1,jugador1.y1,
		jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,
		jugador2.x2,jugador2.y2,
		puntos1,puntos2);
	//std::cout<<"CS: "<<cadCS<<"\n";
	write(tubCS, cadCS, sizeof(cadCS));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	extern int aux1;
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;
				//aux1=1;
				//alarm(5);	
				break;
	case 'w':jugador1.velocidad.y=4;
				//aux1=1;
				//alarm(5);	
				break;
	case 'l':jugador2.velocidad.y=-4;
				//aux1=1;
				//alarm(5);	
				break;
	case 'o':jugador2.velocidad.y=4;
				//aux1=1;
				//alarm(5);	
				break;
	}
}

void CMundo::Init()
{

	//extern int aux1;
	//aux1=1;
	//alarm(5);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;


	//Logger
	tuberia = open ("/tmp/loggerfifo", O_WRONLY);
	/*
	//Bot
	//Fichero proyectado en memoria
	
	//Creación del fichero
	int file = open ("/tmp/datosBot.txt", O_RDWR|O_CREAT|O_TRUNC, 0777); 
	//Escribimos en el fichero
	write(file, &Swap, sizeof(Swap));
	//Asignamos al puntero de proyeccion el lugar donde esta proyectado
	proyection = (char *)mmap(NULL, sizeof(Swap), PROT_WRITE|PROT_READ, MAP_SHARED, file, 0);
	//Cerramos el fichero
	close (file);
	//Asignamos el valor del puntero de proyeccion al puntero de Datos
	pSwap=(DatosMemCompartida*)proyection;
	//Accion por defecto
	pSwap->accion=0;*/

	//CLIENTE-SERVIDOR

	//Abrimos la tuberia de comunicacion entre CLIENTE-SERVIDOR
	tubCS=open("/tmp/csfifo", O_WRONLY);

	//Thread Cliente-Servidor
	pthread_create(&thid1, NULL, hilo_comandos, this);

	//Tuberias teclas
	tubTeclas=open("/tmp/teclasfifo", O_RDONLY);
	


}
