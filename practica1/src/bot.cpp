#include <iostream>
#include <sys/stat.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	//Creo el fichero, el puntero a la memoria y el puntero para la proyeccion
	int file;
	DatosMemCompartida* pSwap;
	char* proyection;

	//Abro el fichero
	file=open("/tmp/datosBot.txt",O_RDWR);

	//Proyecto el fichero
	proyection=(char*)mmap(NULL,sizeof(*(pSwap)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	//Cierro el fichero
	close(file);

	//Apunto el puntero de Datos a la proyeccion del fichero en memoria
	pSwap=(DatosMemCompartida*)proyection;

	//Condicion de salida
	int salir=1;

	//Acciones de control de la raqueta
	while(salir)
	{
		if(pSwap->accion==5)
		{
			salir=0;
		}
		/*
		float posRaqueta;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;

		usleep(25000); 
		*/
		usleep(25000);
		float posRaqueta;
		posRaqueta=( (pSwap->raqueta1.y1+pSwap->raqueta1.y2)/2);
		if(posRaqueta<pSwap->esfera.centro.y)
			pSwap->accion=1;
		else if(posRaqueta>pSwap->esfera.centro.y)
			pSwap->accion=-1;
		else pSwap->accion=0;		

	}
	
	//Desmontamos la proyeccion de memoria
	munmap(proyection,sizeof(*(pSwap)));
}
