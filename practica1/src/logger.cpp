#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	
	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo", O_RDONLY);
	int salir=1;
	int aux;
	while(salir)
	{	
		
		char buff[200];
		aux = read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
		if (aux==-1 || buff [0]=='-' || buff [0]=='0' || aux==0)
		{
			printf("__Tenis Cerrado__\n");	
			salir=0; //Si se da la condicion, salimos del bucle
		}
	}
	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}
